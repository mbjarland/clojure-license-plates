(ns clojure-license-plates.core
  (:require [clojure.java.io :as io]
            [criterium.core :as crit]
            [clj-mmap :as mmap]
            [clojure.core.async :as a
             :refer [>! <! >!! <!! go chan buffer close! thread
                     alts! alts!! timeout]]
            [taoensso.tufte :as tufte
             :refer (defnp p profiled profile)])

  (:import (mxy IntTools)
           (java.util BitSet)
           (clojure.lang Atom)
           ;(java.nio.file Paths Files)
           (java.net URI)
           (java.io FileInputStream)
           (java.nio.channels FileChannel FileChannel$MapMode)
           (java.nio ByteBuffer))
  (:gen-class))

;; functions to generate test files
(def alphabet-length 26)

(def alphabet
  (remove #{\I \Q \V} (mapv (comp char (partial + 65)) (range alphabet-length))))

(defn random-string
  "Returns a random string from alphabet with specified length"
  [length]
  (apply str (take length (repeatedly #(rand-nth alphabet)))))

(defn random-number
  "Returns a random string using digits 0-9 with specified length"
  [length]
  (apply str (take length (repeatedly #(int (rand 10))))))

(defn license-plate-seq
  [list-length string-length]
  (take list-length (repeatedly #(str (random-string string-length)
                                      (random-number string-length)))))

(defn random-license-plate []
  (str (random-string 3) (random-number 3)))

(defn license-plate-seq []
  (repeatedly random-license-plate))

(defn create-sample-file [file-name number-of-items]
  (spit file-name
        (with-out-str (dotimes [n number-of-items]
                        (println (random-license-plate))))))

(defn read-sample-file [file-name]
  (letfn [(helper [rdr]
            (lazy-seq
              (if-let [line (.readLine rdr)]
                (cons line (helper rdr))
                (do (.close rdr) nil))))]
    (helper)))

;; functions to read file to byte array
(defn file-to-byte-array2
  [^java.io.File file]
  (let [result (byte-array (.length file))]
    (with-open [in (java.io.DataInputStream. (clojure.java.io/input-stream file))]
      (.readFully in result))
    result))



(defn file-to-byte-array-bb ^ByteBuffer [^String file-name]
  (with-open [channel (.getChannel (FileInputStream. file-name))]
    (let [len (.size channel)
          mbb (.map channel FileChannel$MapMode/READ_ONLY 0 len)]
      mbb)))
      ;buf.get(bytes, 0, bytes.length);
      ;;(.get mbb ba 0 len)
      ;;ba)))

(defn file-to-byte-array-5 ^ByteBuffer [^String file-name]
  (with-open [channel (.getChannel (FileInputStream. file-name))]
    (let [len (.size channel)
          mbb (.map channel FileChannel$MapMode/READ_ONLY 0 len)
          arr (byte-array len)]
      (.get mbb arr)
      arr)))

  ;;final FileChannel channel = new FileInputStream(fileName).getChannel();
  ;;MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());

  ;;// when finished
  ;;channel.close();

;(defn file-to-byte-array-4 [file-name]
;  (let [path (Paths/get "." (into-array [file-name]))]
;    (Files/readAllBytes path)))

(defn file-to-byte-array-3
  "Slurp the bytes from a slurpable thing"
  [x]
  (with-open [out (java.io.ByteArrayOutputStream.)]
    (clojure.java.io/copy (clojure.java.io/input-stream x) out)
    (.toByteArray out)))

(defn file-to-byte-array2 ^bytes [file-name]
  (let [f (java.io.File. file-name)
        ary (byte-array (.length f))
        is (java.io.FileInputStream. f)]
    (.read is ary)
    (.close is)
    ary))

(defn file-to-byte-array ^bytes [file-name]
  (with-open [mapped-file (mmap/get-mmap file-name)]
    (mmap/get-bytes mapped-file 0 (.size mapped-file))))

(def ^Integer sub
  (unchecked-int
    (-
      (+
        (* 65 676000)
        (* 65 26000)
        (* 65 1000)
        (* 48 100)
        (* 48 10)
        48))))

;public static int asInt(byte[] arr, int offset) {
;  return (arr[offset    ] * 676000)  +
;         (arr[offset + 1] * 26000)   +
;         (arr[offset + 2] * 1000)    +
;         (arr[offset + 3] * 100)     +
;         (arr[offset + 4] * 10)      +
;         (arr[offset + 5])           -
;         sub;
;}

(defn get-plate-str [data n]
  (String. ^bytes data ^long n 6))

(defn get-success-str [data n]
  (str "Dupe at line " (inc (/ n 7)) " - " (get-plate-str data n)))

(defn bb-to-string [^ByteBuffer bb ^long off ^long len]
  (let [arr (byte-array len)]
    (.get bb arr off len)
    (String. arr)))


(defn detect-dupes-bb [file-name]
  (let [data (file-to-byte-array-bb file-name)
        count (.capacity data)
        max (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs (BitSet. max)]
    (loop [n 0]
      (when (< n count)
        (let [val (IntTools/asIntBB data n)]
          (if (.get bs val)
            (println "Dupe at line" (inc n) "-") ;; (bb-to-string data n 6))
            (do (.set bs val)
                (recur (+ n 7)))))))))

;; functions to detect dupes
(defn detect-dupes [file-name]
  (let [data (file-to-byte-array-5 file-name)
        ^long count (alength data)
        max (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs (BitSet. max)]
    (loop [n 0]
      (when (< n count)
        (let [val (IntTools/asInt data n)]
          (if (.get bs val)
            (println "Dupe at line" (inc n) "-" (String. ^bytes data n 6))
            (do (.set bs val)
                (recur (+ n 7)))))))))

(tufte/add-basic-println-handler! {})

(defn create-ints [file-name]
  (let [data (file-to-byte-array file-name)]
    (IntTools/getIntArray data)))

(defn detect-dupes-in-shard [^bytes data
                             [^long tn ^long ti]
                             ^Atom result
                             ^Atom done]
  (let [count (count data)
        max (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs (BitSet. max)
        batch (* 7 tn)]
    (thread
      (loop [n (* 7 ti)]
        (when (and (< n count) (not @result))
          (let [val (IntTools/asInt data n)]
            (if (.get bs val)
              (reset! result (str "Dupe at line " (inc n) " - " (get-plate-str data n)))
              (do (.set bs val)
                  (recur (+ n batch)))))))
      (swap! done inc))
    bs))

(defn detect-dupes-in-shard2 [^bytes data
                             [^long tn ^long ti]
                             ^Atom result
                             ^Atom done]
  (let [count (count data)
        max (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs (BitSet. max)
        batch (/ count tn)
        start (* batch ti)
        end   (dec (+ start batch ))]
    (thread
      (profile
        {}
        (p :loop
      (loop [n start]
        (when (and (p :c-end (< n end)) (p :c-result (not @result)))
          (let [val (p :as-int (IntTools/asInt data n))]
            (if (p :bs-get (.get bs val))
              (p :reset (reset! result (str "Dupe at line " (inc n) " - " (get-plate-str data n))))
              (do (p :bs-set (.set bs val))
                  (recur (+ n 7)))))))))
      (swap! done inc))
    bs))


(defn get-result-or-nil [bss]
  (let [res (reduce
      (fn [a b]
        (if (.intersects a b)
          (do (.and a b)
              (reduced (str "Dupe - " (IntTools/asString (.nextSetBit a 0)))))
          (do (.or b a)
              b)))
      bss)]
    (if (instance? String res)
      res
      false)))

(defn detect-dupes-sharded
  ([file-name] (detect-dupes-sharded file-name 8))
  ([file-name ^long threads]
   (let [data (file-to-byte-array file-name)
         result (atom false)
         done (atom 0)
         bss (doall
               (map #(detect-dupes-in-shard2 data
                                            [threads %]
                                            result
                                            done)
               (range threads)))]
     (while (not= @done threads)
       (Thread/sleep 1))
     (or
       @result
       (get-result-or-nil bss)
       "no dupes found"))))

(defn -main [& args]
  (when (seq args))
  (let [result (with-out-str
                 (time (detect-dupes (first args))))]
    (println result)))

(defn detect-dupes-dotimes [file-name]
  (let [data (file-to-byte-array (clojure.java.io/file file-name))
        count (count data)
        size (/ count 7)
        max (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs (BitSet. max)]
    (try
      (dotimes [n size]
        (let [off (* n 7)
              val (IntTools/asInt data off)]
          (if (.get bs val)
            (throw (Exception. (str "Dupe at line " (inc n) " - " (String. ^bytes data off 6))))
            (.set bs val))))
      (catch Exception e (.getMessage e)))))

(defn detect-dupes-lazy [file-name]
  (let [data (file-to-byte-array (clojure.java.io/file file-name))
        count (count data)
        max (IntTools/asInt (.getBytes "ZZZ999") 0)
        bs (BitSet. max)]
    (for [n (range 0 count 7)
          :let [val (IntTools/asInt data n)]
          :when (let [c (.get bs val)] (.set bs val) c)]
      (let
        [plate (String. ^bytes data n 6)
         line (inc (/ n 7))]
        {:found   true
         :line    line
         :plate   plate
         :message (str "Dupe at line " line " - " plate)
         :offset  n}))))

(defn detect-dupes-for [file-name]
  (or (first (detect-dupes-lazy file-name))
      {:found false :message "No dupes found"}))

;
;(defn detect-dupes-reduce [file-name]
;  (let [data  (file-to-byte-array (clojure.java.io/file file-name))
;        count (count data)
;        max   (IntTools/asInt (.getBytes "ZZZ999") 0)
;        bs    (BitSet. max)]
;    (reduce
;      (fn [bs n]
;        (when (>= n count) (reduced "No dupes!"))
;        (let [val (IntTools/asInt data n)]
;          (if (.get bs val)
;            (reduced (str "Dupe at line " (inc n) " - " (String. ^bytes data n 6)))
;            (do (.set bs val)
;                bs))))
;      bs
;      (range 0 count 7))))
;
;(defn detect-dupes-some [file-name]
;  (let [data  (file-to-byte-array (clojure.java.io/file file-name))
;        count (count data)
;        max   (IntTools/asInt (.getBytes "ZZZ999") 0)
;        bs    (BitSet. max)]
;    (some
;      (fn [n]
;        (when (< n count)
;          (let [val (IntTools/asInt data n)]
;            (if (.get bs val)
;              (str "Dupe at line " (inc n) " - " (String. ^bytes data n 6))
;              (do
;                (.set bs val)
;                false)))))
;      (range 0 count 7))))
