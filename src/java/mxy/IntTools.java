package mxy;

import java.nio.ByteBuffer;

public class IntTools {
  private static int sub =
      65 * 676000 +
      65 * 26000 +
      65 * 1000 +
      48 * 100 +
      48 * 10 +
      48; // 45700328


  public static int[] getIntArray(byte[] arr) {
    int len = arr.length;
    //int[] result = new int[len / 7];

    for (int i=0; i<len; i += 7) {
      asInt(arr, i);
    }

    return null;
  }

  public static int asInt(byte[] arr, int offset) {
    return (arr[offset] * 676000) +
      (arr[offset + 1] * 26000) +
      (arr[offset + 2] * 1000) +
      (arr[offset + 3] * 100) +
      (arr[offset + 4] * 10) +
      (arr[offset + 5]) -
      sub;
  }

  public static int asIntBB(ByteBuffer bb, int offset) {
    return (bb.get(offset) * 676000) +
      (bb.get(offset + 1) * 26000) +
      (bb.get(offset + 2) * 1000) +
      (bb.get(offset + 3) * 100) +
      (bb.get(offset + 4) * 10) +
      (bb.get(offset + 5)) -
      sub;
  }

  public static String asString(int coded) {
    byte[] buf = new byte[6];

    byte b5 = (byte) (coded % 10);
    buf[5]  = (byte) (b5 + 48); //(+ (mod (+ 28123) 10) 48)
    coded -= b5;
    coded /= 10;

    byte b4 = (byte) (coded % 10);
    buf[4]  = (byte) (b4 + 48); //(+ (mod (+ 28123) 10) 48)
    coded -= b4;
    coded /= 10;

    byte b3 = (byte) (coded % 10);
    buf[3]  = (byte) (b3 + 48); //(+ (mod (+ 28123) 10) 48)
    coded -= b3;
    coded /= 10;

    byte b2 = (byte) (coded % 26);
    buf[2]  = (byte) (b2 + 65); //(+ (mod (+ 28123) 10) 48)
    coded -= b2;
    coded /= 26;

    byte b1 = (byte) (coded % 26);
    buf[1]  = (byte) (b1 + 65); //(+ (mod (+ 28123) 10) 48)
    coded -= b1;
    coded /= 26;

    buf[0]  = (byte) (coded + 65); //(+ (mod (+ 28123) 10) 48)

    return new String(buf);
  }

}
